<?php
/**
 * Determine what level in the menu tree the current page resides.
 * @return int
 */
function _widget_pack_menu_level($menu = 'main-menu') {
  $tree = menu_tree_page_data($menu);

  $level = 0;
  $paths = array();
// Go down the active trail as far as possible.
  while ($tree) {
// Loop through the current level's items until we find one that is in trail.
    while ($item = array_shift($tree)) {
      if ($item['link']['in_active_trail']) {
// If the item is in the active trail, we count a new level.
        $level++;
        $paths[] = $item['link']['link_path'];
        if (!empty($item['below'])) {
// If more items are available, we continue down the tree.
          $tree = $item['below'];
          break;
        }
// If we are at the end of the tree, our work here is done.
        break 2;
      }
    }
  }

  return $level;
}





function _widget_pack_menu_form($options, $input = 'microsite') {


  // Retrieve the settings.
  $settings = variable_get('widget_pack');

  // Set options defaults if necessary

  $form = array();



  $form['microsite'] = array(
    '#title' => 'Enable microsite',
    '#description' => 'If checked all pages beneath this menu item will be rendered as a microsite.',
    '#type' => 'checkbox',
    '#default_value' => $options['microsite'],
  );



  $form['microsite_wrapper'] = array(
    '#title' => 'Microsite settings',
    '#description' => '',
    '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="' . $input . '"]' => array('checked' => TRUE),

      ),

    ),

  );

  if ($input != 'microsite') {
    $form['microsite']['#states'] = array(
      'visible' => array(
        ':input[name="menu[enabled]"]' => array('checked' => TRUE),
      ),
    );

    $form['microsite_wrapper']['#states']['visible'][':input[name="menu[enabled]"]']  =  array('checked' => TRUE);

    // Add to the description if on the node create page.

    // Find all the microsites enabled for which menus.
    $enabled_menus = array();
    if (isset($settings['menu'])) {
      foreach (array_keys($settings['menu']) as $value) {
        if ($settings['menu'][$value]['microsite']['enable']) {
          $enabled_menus[] = $value;
        }
      }
    }


    // FInd all the available menus for this page.
    $available_menus = array();
    $type_menus = variable_get('menu_options_' . $options['node'], array());
    foreach ($type_menus as $menu) {
      $available_menus[] = $menu;
    }


    $microsite_menus = array_intersect($enabled_menus, $available_menus);

    if (empty($microsite_menus)) {
      return array();
    }

    $form['microsite']['#description'] .= '<br/><em>Please note: Microsites are only enabled for the following menus. <strong>' . implode(', ',$microsite_menus) . '</strong>.</em>';


  }



  $form['microsite_wrapper']['microsite_columns'] = array(
    '#title' => 'How many columns of links?',
    '#description' => 'Reorder navigation links into columns.',
    '#type' => 'select',
    '#options' => array(
      '1' => '1',
      '2' => '2',
      '3' => '3'
    ),
    '#default_value' => $options['microsite_columns'],
    '#weight' => 3,
    '#states' => array(
      'visible' => array(
        ':input[name="' . $input . '"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['microsite_wrapper']['microsite_prev_next'] = array(
    '#title' => 'Show previous and next links?',
    '#description' => 'Display links to the next and previous pages in the microsite.',
    '#type' => 'select',
    '#options' => array(
      1 => 'Yes',
      0 => 'No'
    ),
    '#default_value' => $options['microsite_prev_next'],
    '#weight' => 4,
    '#states' => array(
      'visible' => array(
        ':input[name="' . $input . '"]' => array('checked' => TRUE),
      ),
    ),
  );


  $form['microsite_wrapper']['microsite_type'] = array(
    '#title' => 'Custom CSS class',
    '#description' => 'Select the microsite type you wish to display.',
    '#type' => 'textfield',
    '#default_value' => $options['microsite_type'],
    '#weight' => 5,
    '#size' => 10,
    '#states' => array(
      'visible' => array(
        ':input[name="' . $input . '"]' => array('checked' => TRUE),
      ),
    ),
  );


  $form['microsite_wrapper']['microsite_title'] = array(
    '#title' => 'Title override',
    '#description' => 'Overides the title of the microsite page when rendered in the naviagation e.g. Overview or Home. Leave blank to use the original.',
    '#type' => 'textfield',
    '#default_value' => $options['microsite_title'],
    '#weight' => 5,
    '#size' => 10,
    '#states' => array(
      'visible' => array(
        ':input[name="' . $input . '"]' => array('checked' => TRUE),
      ),
    ),
  );

  return $form;
}

function widget_pack_form_alter(&$form, &$form_state, $form_id) {


  // Retrieve the settings.
  $settings = variable_get('widget_pack');


  if ($form_id == 'menu_edit_item') {

    if (isset($form['mlid']['#value'])) {
      $link = menu_link_load($form['mlid']['#value']);
      $options = $link['options'];
    }

    // Extract the microsite settings for this menu.
    $microsite_settings = isset($settings['menu']) ? $settings['menu'][$link['menu_name']]['microsite'] : array();

    if (isset($microsite_settings['enable']) && $microsite_settings['enable']) {

      // Add some defaults
      $options += array(
        'microsite' => 0,
        'microsite_type' => 'links',
        'microsite_columns' => 1,
        'microsite_prev_next' => 1,
        'microsite_title' => '',
      );

      // Add options for campaign page or microsite.
      $form += _widget_pack_menu_form($options);

      $form['#submit'][] = 'widget_pack_menu_edit_item_submit';
    }

  }


}


/**
 * Implements hook_form_FORMID_alter().
 */
function widget_pack_form_node_form_alter(&$form, &$form_state, $form_id) {

  if (isset($form['menu'])) {
    if ($form['menu']['link']['mlid']['#value']) {

      $link = menu_link_load($form['menu']['link']['mlid']['#value']);
      $options = $link['options'];
    } else {
      $options = array();
    }

// Set some defaults!!
      $options += array(
        'microsite' => 0,
        'microsite_type' => 'links',
        'microsite_columns' => 1,
        'microsite_prev_next' => 1,
        'microsite_title' => '',
      );

    // Add the node type to the options.
    $options['node'] = $form['#node']->type;

    // Add options for campaign page or microsite.
    $form['menu'] += _widget_pack_menu_form($options, 'menu[microsite]');


// Add a submit handler
//  $form['#submit'][] = 'widget_pack_node_form_submit';
  }

}


/**
 * @param $form
 * @param $form_state
 */
function widget_pack_menu_edit_item_submit($form, &$form_state) {

  $link = menu_link_load($form_state['values']['mlid']);
  $link['options']['microsite'] = $form_state['values']['microsite'];
  $link['options']['microsite_type'] = $form_state['values']['microsite_type'];
  $link['options']['microsite_columns'] = $form_state['values']['microsite_columns'];
  $link['options']['microsite_prev_next'] = $form_state['values']['microsite_prev_next'];
  $link['options']['microsite_title'] = $form_state['values']['microsite_title'];
  menu_link_save($link);

}


/**
 * Implements hook_node_insert().
 */
function widget_pack_node_insert($node) {
  widget_pack_node_save($node);
}

/**
 * Implements hook_node_update().
 */
function widget_pack_node_update($node) {
  widget_pack_node_save($node);
}


/**
 * Helper for hook_node_insert() and hook_node_update().
 */
function widget_pack_node_save($node) {

// @todo check if this is enabled for this content type.
  if (isset($node->menu) && isset($node->menu['enabled']) && $node->menu['enabled']) {
    $link = menu_link_load($node->menu['mlid']);
    if (isset($node->menu['microsite'])) {
      $link['options']['microsite'] = $node->menu['microsite'];
      $link['options']['microsite_type'] = $node->menu['microsite_wrapper']['microsite_type'];
      $link['options']['microsite_columns'] = $node->menu['microsite_wrapper']['microsite_columns'];
      $link['options']['microsite_prev_next'] = $node->menu['microsite_wrapper']['microsite_prev_next'];
      $link['options']['microsite_title'] = $node->menu['microsite_wrapper']['microsite_title'];
      menu_link_save($link);
    }
  }

}



function _widget_pack_generate_menu() {

// Retrieve the parent menu item from the active trail.
  $active_trail = menu_get_active_trail();
  $last_menu_item = array_pop($active_trail);
  $menu_parent = end($active_trail);

// Check if the current page is a microsite.
  if ($last_menu_item['options']['microsite'] == 1) {


    $config = array(
      'menu_name' => $last_menu_item['menu_name'],
      'parent_mlid' => 0,
      'sort' => FALSE,
      'follow' => 1,
      'level' => _widget_pack_menu_level() + 1,
      'depth' => 1,
      'expanded' => TRUE,
      'title_link' => FALSE,
      'delta' => 'microsite',
    );

    $parent_config = $config;
    $parent_config['level']--;
    $parent_menu_tree = menu_tree_build($parent_config);

    // Extract the parent link out of the array.
    $parent_link = $parent_menu_tree['content']['#content'][$last_menu_item['mlid']];

    $microsite_columns = $last_menu_item['options']['microsite_columns'];
    $microsite_type = $last_menu_item['options']['microsite_type'];
    $microsite_title = $last_menu_item['options']['microsite_title'];
    $microsite_prev_next = $last_menu_item['options']['microsite_prev_next'];

  }
  elseif ($menu_parent['options']['microsite'] == 1) {


    $config = array(
      'menu_name' => $last_menu_item['menu_name'],
      'parent_mlid' => 0,
      'sort' => FALSE,
      'follow' => 1,
      'level' => _widget_pack_menu_level(),
      'depth' => 1,
      'expanded' => TRUE,
      'title_link' => FALSE,
      'delta' => 'microsite',
    );


    $parent_config = $config;
    $parent_config['level']--;
    $parent_menu_tree = menu_tree_build($parent_config);

    // Extract the parent link out of the array.
    $parent_link = $parent_menu_tree['content']['#content'][$menu_parent['mlid']];

    $microsite_columns = $menu_parent['options']['microsite_columns'];
    $microsite_type = $menu_parent['options']['microsite_type'];
    $microsite_title = $menu_parent['options']['microsite_title'];
    $microsite_prev_next = $menu_parent['options']['microsite_prev_next'];

  }
  else {
    return NULL;
  }


  // Overide the title if necessary
  if ($microsite_title != '') {
    $parent_link['#title'] = $microsite_title;
  }

  // Build the menu tree via menu block module.
  $menu_tree = menu_tree_build($config);

  // Extract all the links
  $existing_links = array();
  foreach (element_children($menu_tree['content']['#content']) as $key => $value) {
    // ADd to a temporary array
    $existing_links[$value] = $menu_tree['content']['#content'][$value];
    // Remove it from the source
    unset($menu_tree['content']['#content'][$value]);
  }

  // Add the parent link to the render array
  $menu_tree['content']['#content'][$parent_link['#original_link']['mlid']] = $parent_link;

  // Add back in the existing links.
  foreach ($existing_links as $key => $value) {
    $menu_tree['content']['#content'][$key] = $value;
  }

  return array(
    'menu' => $menu_tree,
    'parent' => $parent_link,
    'columns' => $microsite_columns,
    'type' => $microsite_type,
    'title' => $microsite_title,
    'prev_next' => $microsite_prev_next
  );


}

/**
 * Content callback for the microsite_prev_next block.
 */
function _widget_pack_microsite_prev_next() {

  $microsite =  _widget_pack_generate_menu();

  // Only display the navigation if the option is set.
  if (!$microsite['prev_next']) {
    return NULL;
  }

  // Build the menu tree via menu block module.
  $menu_tree = $microsite['menu'];

  $menu_links = element_children($menu_tree['content']['#content']);

  $this_page = false;
  // find the current page in the menu tree
  foreach (element_children($menu_tree['content']['#content']) as $key) {
    if ($menu_tree['content']['#content'][$key]['#href'] == current_path()) {
      $this_page = $key;
    }
  }

  // If the current page is not in the menu then hide the block.
  if (!$this_page) {
    return NULL;
  }

  $prev = NULL;
  $next = NULL;

  $current_position = array_search($this_page, $menu_links);

  if (isset($menu_links[$current_position - 1])) {
    $prev = menu_link_load($menu_links[$current_position - 1]);
  }
  if (isset($menu_links[$current_position + 1])) {
    $next = menu_link_load($menu_links[$current_position + 1]);
  }

  // FAPI
  $form = array();

  // Attach some css
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'widget_pack') . '/css/microsite.css',
  );

  // Container
  $form['prev_next'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'prev-next'
    )
  );

  // If we have a previous link.
  if ($prev) {
    $form['prev_next']['prev'] = array(
      '#markup' => theme('prev_next_link',array('title' => t('Previous'), 'subtitle' => $prev['link_title'], 'url' => $prev['link_path'], 'class' => array('prev')))
    );
  }

  // If we have a next link.
  if ($next) {
    $form['prev_next']['next'] = array(
      '#markup' => theme('prev_next_link',array('title' => t('Next'), 'subtitle' => $next['link_title'], 'url' => $next['link_path'], 'class' => array('next')))

    );
  }

  // Return the block.
  return $form;
}

function _widget_pack_microsite_links() {

  $microsite =  _widget_pack_generate_menu();

// Deal with the columns

  $form = array();
  $form['container'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'microsite'
      )
    )
  );
  if ($microsite['type'] != '') {
    $form['container']['#attributes']['class'][] = $microsite['type'];
  }

  $form['container']['content'] = $microsite['menu']['content']['#content'];

  return $form;

}

/**
 * Check if microsites are enabled for a particular menu
 * @param $menu
 */
function _widget_pack_microsite_enabled($menu) {
  $settings = variable_get('widget_pack');
  if (isset($settings['menu']) && isset($settings['menu'][$menu]['microsite']['enable']) && $settings['menu'][$menu]['microsite']['enable'] == 1) {
    return TRUE;
  }
  return FALSE;
}