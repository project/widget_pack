<?php print l(t('<span class="prev-next-title">' . $title . '</span><span class="prev-next-subtitle">' . $subtitle . '</span>'), $url, array(
  'html' => TRUE,
  'attributes' => array('class' => $class)
));
