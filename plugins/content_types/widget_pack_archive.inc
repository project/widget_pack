<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Inserted Archive'),
  'description' => t('Add a archive.'),
  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('widget_pack_archive_content_type'),
  // Name of a function which will render the block.
  'render callback' => 'widget_pack_archive_content_type_render',
  // The default context.
  'defaults' => array(),
  'admin info' => 'widget_pack_archive_admin_info',
  // This explicitly declares the config form. Without this line, the func would be
  // ctools_plugin_example_no_context_content_type_edit_form.
  'edit form' => 'widget_pack_archive_content_type_edit_form',
  'category' => array(t('Widgets'), -9),
  // this example does not provide 'admin info', which would populate the
  // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function widget_pack_archive_content_type_render($subtype, $conf, $args, $context) {
  global $language;

  $block = new stdClass();


  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $conf['bundle'])
    ->propertyCondition('status', NODE_PUBLISHED);

  $result = $query->execute();
  if (isset($result['node'])) {
    $nids = array_keys($result['node']);
    $posts = entity_load('node', $nids);
  }

  $post_dates = array();

  foreach ($posts as $key => $post) {
    $year = date('Y', $post->created);
    $month = date('m', $post->created);
    if (!isset($post_dates[$year])) {
      $post_dates[$year] = array();
    }
    if (!isset($post_dates[$year][$month])) {
      $post_dates[$year][$month] = 0;
    }
    $post_dates[$year][$month]++;
  }

  krsort($post_dates, SORT_NUMERIC);

  $dates = array();
  foreach ($post_dates as $year => $months) {
    ksort($months, SORT_NUMERIC);
    foreach ($months as $month => $count) {
      $month_obj = DateTime::createFromFormat('!m', $month);
      $month_name = $month_obj->format('F');
      $dates[] = l($month_name . ' ' . $year . ' ' . '(' . $count . ')', $conf['base'] . '/' . $conf['bundle'] . '/' . $year . '/' . $month);
    }
  }



  $block->content = theme('item_list', array('items' => $dates));
  return $block;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function widget_pack_archive_content_type_edit_form($form, &$form_state) {

  // Load the saved settings.  
  $conf = $form_state['conf'];


  // Choose the bundle

  $types_array = array_keys(node_type_get_types());
  $types_array = array_combine($types_array,$types_array);



//
  $form['bundle'] = array(
    '#title' => 'Choose content bundle',
    '#type' => 'select',
    '#options' => $types_array,
    '#default_value' => $conf['bundle']
  );

  // year only
  // year + month
  // years with months nested


  $form['count'] = array(
    '#title' => 'Include post count?',
    '#description' => 'Optionally include a post count',
    '#type' => 'checkbox',
    '#default_value' => $conf['count']
  );

  $form['base'] = array(
    '#title' => 'Base path',
    '#description' => 'Enter the base path. Default: archive/',
    '#type' => 'textfield',
    '#default_value' => isset($conf['base']) ?  $conf['base'] : '/archive'
  );






  // return the renderable array.
  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function widget_pack_archive_content_type_edit_form_submit(&$form, &$form_state) {
  $keys = array('bundle','count','base');


  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


/**
 * Ctools plugin function hook_admin_info.
 */
function widget_pack_archive_admin_info($subtype, $conf, $context = NULL) {

  if (empty($output) || !is_object($output)) {
    $output = new stdClass();
//    $node = node_load($conf[$conf['bundle'] . '_type']);

 //   $output->title = $conf['bundle'] . ' : ' . $node->title . '';
    $output->content = t('No info available.');
  }

  return $output;
}

