<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Horizontal Rule'),
  'description' => t('Add a horizontal rule with style options'),
  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('widget_pack_hr_content_type'),
  // Name of a function which will render the block.
  'render callback' => 'widget_pack_hr_render',
  // The default context.
  'defaults' => array(),
  'admin info' => 'widget_pack_hr_admin_info',
  // This explicitly declares the config form. Without this line, the func would be
  // ctools_plugin_example_no_context_content_type_edit_form.
  'edit form' => 'widget_pack_hr_edit_form',
  'category' => array(t('Widgets'), -9),
  // this example does not provide 'admin info', which would populate the
  // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function widget_pack_hr_render($subtype, $conf, $args, $context) {
  $classes = array('horizontal-rule', $conf['style']);
  $class = implode(' ', $classes);
  $output = new stdClass();
  $output->content = '<hr class="' . $class . '"/>';

  return $output;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function widget_pack_hr_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['style'] = array(
    '#type' => 'select',
    '#title' => t('Thickness'),
    '#options' => array(
      'normal' => t('Normal'),
      'thin' => t('Thin'),
    ),
    '#default_value' => $conf['style'],
    '#description' => t('Select the line thickness.'),
  );

  // We dont want a title
  $form['override_title']['#access'] = FALSE;
  $form['override_title_text']['#access'] = FALSE;
  $form['override_title_markup']['#access'] = FALSE;

  // return the renderable array.
  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function widget_pack_hr_edit_form_submit(&$form, &$form_state) {
  $keys = array('style');
  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


/**
 * Ctools plugin function hook_admin_info.
 */
function widget_pack_hr_admin_info($subtype, $conf, $context = NULL) {
  $output = new stdClass();
  $output->title = 'Horizontal rule';
  return $output;
}

