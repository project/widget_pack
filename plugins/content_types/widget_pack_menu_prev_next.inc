<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Menu previous/next links'),
  'description' => t('Previous & Next links'),
  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('widget_pack_menu_prev_next_content_type'),
  // Name of a function which will render the block.
  'render callback' => 'widget_pack_menu_prev_next_content_type_render',
  // The default context.
  'defaults' => array(),
  'admin info' => 'widget_pack_menu_prev_next_admin_info',
  // This explicitly declares the config form. Without this line, the func would be
  // ctools_plugin_example_no_context_content_type_edit_form.
  'edit form' => 'widget_pack_menu_prev_next_content_type_edit_form',
  'category' => array(t('Widgets'), -9),
  // this example does not provide 'admin info', which would populate the
  // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function widget_pack_menu_prev_next_content_type_render($subtype, $conf, $args, $context) {


  // @todo allow whether this should be shown or not based on an external piece of data



  $menu = $conf['menu'];

  // Create a new block object.
  $block = new stdClass();

  $config = array(
    'menu_name' => $menu,
    'parent_mlid' => 0,
    'sort' => FALSE,
    'follow' => 1,
    'level' => _widget_pack_menu_level(),
    'depth' => 1,
    'expanded' => TRUE,
    'title_link' => FALSE,
    'delta' => 'prev_next',
  );

  // Build the menu tree via menu block module.
  $menu_tree = menu_tree_build($config);

  $menu_links = element_children($menu_tree['content']['#content']);

  $this_page = false;
  // find the current page in the menu tree
  foreach (element_children($menu_tree['content']['#content']) as $key) {
    if ($menu_tree['content']['#content'][$key]['#href'] == current_path()) {
      $this_page = $key;
    }
  }

  // If the current page is not in the menu then hide the block.
  if (!$this_page) {
    return NULL;
  }

  $prev = NULL;
  $next = NULL;

  $current_position = array_search($this_page, $menu_links);

  if (isset($menu_links[$current_position - 1])) {
    $prev = menu_link_load($menu_links[$current_position - 1]);
  }
  if (isset($menu_links[$current_position + 1])) {
    $next = menu_link_load($menu_links[$current_position + 1]);
  }

  // FAPI
  $form = array();

  // Container
  $form['prev_next'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'prev-next'
    )
  );

  // If we have a previous link.
  if ($prev) {
    $form['prev_next']['prev'] = array(
      '#markup' => theme('prev_next_link',array('title' => t('Previous'), 'subtitle' => $prev['link_title'], 'url' => $prev['link_path'], 'class' => array('prev')))
    );
  }

  // If we have a next link.
  if ($next) {
    $form['prev_next']['next'] = array(
      '#markup' => theme('prev_next_link',array('title' => t('Next'), 'subtitle' => $next['link_title'], 'url' => $next['link_path'], 'class' => array('next')))

    );
  }

  $block->content = $form;

  // Return the block.
  return $block;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function widget_pack_menu_prev_next_content_type_edit_form($form, &$form_state) {

  // Load the saved settings.  
  $conf = $form_state['conf'];

  $menus = menu_get_menus();

  $form['menu'] = array(
    '#title' => t('Choose a menu'),
    '#description' => t('Select a menu to create the previous next links.'),
    '#type' => 'select',
    '#options' => $menus,
    '#default_value' => $conf['menu']
  );


  // We dont want a title
  $form['override_title']['#access'] = FALSE;
  $form['override_title_text']['#access'] = FALSE;
  $form['override_title_markup']['#access'] = FALSE;
  $form['override_title_heading']['#access'] = FALSE;

  // return the renderable array.
  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function widget_pack_menu_prev_next_content_type_edit_form_submit(&$form, &$form_state) {
  $keys = array('menu');

  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


/**
 * Ctools plugin function hook_admin_info.
 */
function widget_pack_menu_prev_next_admin_info($subtype, $conf, $context = NULL) {

  if (empty($output) || !is_object($output)) {
    $output = new stdClass();
    $output->content = t('No info available.');
  }

  return $output;
}

