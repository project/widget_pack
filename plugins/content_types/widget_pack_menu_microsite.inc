<?php


/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Menu microsite links'),
  'description' => t('Previous & Next links'),
  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('widget_pack_menu_microsite_content_type'),
  // Name of a function which will render the block.
  'render callback' => 'widget_pack_menu_microsite_content_type_render',
  // The default context.
  'defaults' => array(),
  'admin info' => 'widget_pack_menu_microsite_admin_info',
  // This explicitly declares the config form. Without this line, the func would be
  // ctools_plugin_example_no_context_content_type_edit_form.
  'edit form' => 'widget_pack_menu_microsite_content_type_edit_form',
  'category' => array(t('Widgets'), -9),
  // this example does not provide 'admin info', which would populate the
  // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function widget_pack_menu_microsite_content_type_render($subtype, $conf, $args, $context) {

  $menu = $conf['menu'];

  // Create a new block object.
  $block = new stdClass();

  // Retrieve the parent menu item from the active trail.
  $active_trail = menu_get_active_trail();
  $last_menu_item = array_pop($active_trail);
  $menu_parent = end($active_trail);

  // Check if the current page is a microsite.
  if ($last_menu_item['options']['microsite'] == 1) {


    $config = array(
      'menu_name' => $menu,
      'parent_mlid' => 0,
      'sort' => FALSE,
      'follow' => 1,
      'level' => _widget_pack_menu_level() + 1,
      'depth' => 1,
      'expanded' => TRUE,
      'title_link' => FALSE,
      'delta' => 'microsite',
    );

    $parent_config = $config;
    $parent_config['level']--;
    $parent_menu_tree = menu_tree_build($parent_config);

    // Extract the parent link out of the array.
    $parent_link = $parent_menu_tree['content']['#content'][$last_menu_item['mlid']];

    $microsite_columns = $last_menu_item['options']['microsite_columns'];

  } elseif ( $menu_parent['options']['microsite'] == 1 ) {


    $config = array(
      'menu_name' => $menu,
      'parent_mlid' => 0,
      'sort' => FALSE,
      'follow' => 1,
      'level' => _widget_pack_menu_level() ,
      'depth' => 1,
      'expanded' => TRUE,
      'title_link' => FALSE,
      'delta' => 'microsite',
    );


    $parent_config = $config;
    $parent_config['level']--;
    $parent_menu_tree = menu_tree_build($parent_config);

    // Extract the parent link out of the array.
    $parent_link = $parent_menu_tree['content']['#content'][$menu_parent['mlid']];

    $microsite_columns = $menu_parent['options']['microsite_columns'];


  }

  // Build the menu tree via menu block module.
  $menu_tree = menu_tree_build($config);
  // Add the parent link in right at the start of the menu
  array_unshift($menu_tree['content']['#content'],$parent_link);



  // Deal with the columns



  $block->content = $menu_tree['content']['#content'];

  // Return the block.
  return $block;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function widget_pack_menu_microsite_content_type_edit_form($form, &$form_state) {

  // Load the saved settings.  
  $conf = $form_state['conf'];

  $menus = menu_get_menus();

  $form['menu'] = array(
    '#title' => t('Choose a menu'),
    '#description' => t('Select a menu to create the previous next links.'),
    '#type' => 'select',
    '#options' => $menus,
    '#default_value' => $conf['menu']
  );


  // We dont want a title
  $form['override_title']['#access'] = FALSE;
  $form['override_title_text']['#access'] = FALSE;
  $form['override_title_markup']['#access'] = FALSE;
  $form['override_title_heading']['#access'] = FALSE;

  // return the renderable array.
  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function widget_pack_menu_microsite_content_type_edit_form_submit(&$form, &$form_state) {
  $keys = array('menu');

  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


/**
 * Ctools plugin function hook_admin_info.
 */
function widget_pack_menu_microsite_admin_info($subtype, $conf, $context = NULL) {

  if (empty($output) || !is_object($output)) {
    $output = new stdClass();
    $output->content = t('No info available.');
  }

  return $output;
}

