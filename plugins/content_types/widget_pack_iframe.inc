<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
    'title' => t('iframe Embed'),
    'description' => t('Add an iframe to a page'),
    // 'single' => TRUE means has no subtypes.
    'single' => TRUE,
    // Constructor.
    'content_types' => array('widget_pack_iframe_content_type'),
    // Name of a function which will render the block.
    'render callback' => 'widget_pack_iframe_content_type_render',
    // The default context.
    'defaults' => array(),
    'admin info' => 'widget_pack_iframe_admin_info',
    // This explicitly declares the config form. Without this line, the func would be
    // ctools_plugin_example_no_context_content_type_edit_form.
    'edit form' => 'widget_pack_iframe_content_type_edit_form',
    'category' => array(t('Widgets'), -9),
        // this example does not provide 'admin info', which would populate the
        // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function widget_pack_iframe_content_type_render($subtype, $conf, $args, $context) {

  $block = new stdClass();

  // Build attributes array
  $attributes = array();




  if (!isset($conf['width'])) {
    $attributes['width'] = '100%';
  } else {
    $attributes['width'] = $conf['width'];
  }
  if (!isset($conf['height'])) {
    $attributes['height'] = '701';
  } else {
    $attributes['height'] = $conf['height'];
  }
  if (!isset($conf['frameborder']) || $conf['frameborder'] < 0) {
    $attributes['frameborder'] = 0;
  } else {
    $attributes['frameborder'] = $conf['frameborder'];
  }
  if (!isset($conf['scrolling']) || empty($conf['scrolling'])) {
    $attributes['scrolling'] = 'auto';
  } else {
    $attributes['scrolling'] = $conf['scrolling'];
  }
  if (!isset($conf['transparency']) || $conf['transparency'] < 0) {
    $attributes['transparency'] = 0;
  } else {
    $attributes['transparency'] = $conf['transparency'];
  }



  $block->content =  theme('iframe',array('url' => $conf['url'],'title' => $conf['title'],'attributes' => $attributes));
  return $block;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function widget_pack_iframe_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $languages = language_list();


  foreach ($languages as $key => $value) {
    $form[$key] = array(
      '#type' => 'textarea',
      '#title' => t($value->name),
      '#default_value' => $conf[$key]
    );
  }


  $form['override_title']['#access'] = false;
  $form['override_title_text']['#access'] = false;
  $form['override_title_markup']['#access'] = false;
  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function widget_pack_iframe_content_type_edit_form_submit(&$form, &$form_state) {
  $languages = language_list();
  $keys = array_keys($languages);
  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Ctools plugin function hook_admin_info.
 */
function widget_pack_iframe_admin_info($subtype, $conf, $context = NULL) {



  if (empty($output) || !is_object($output)) {
    $output = new stdClass();
    $output->title = 'You have selected a call to action: ' ;
    $output->content = t('No info available.');
  }

  return $output;
}

