<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('List of content'),
  'description' => t('Add a piece of content to the page.'),
  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('widget_pack_list_content_type'),
  // Name of a function which will render the block.
  'render callback' => 'widget_pack_list_content_type_render',
  // The default context.
  'defaults' => array(),
  'admin info' => 'widget_pack_list_admin_info',
  // This explicitly declares the config form. Without this line, the func would be
  // ctools_plugin_example_no_context_content_type_edit_form.
  'edit form' => 'widget_pack_list_content_type_edit_form',
  'category' => array(t('Widgets'), -9),
  // this example does not provide 'admin info', which would populate the
  // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function widget_pack_list_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  $block->content = NULL;

  // Build the initial query.
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $conf['bundle'])
    ->propertyCondition('status', 1);


  // Go through each of the vocabs
  foreach ($conf[$conf['bundle'] . '_vocab'] as $field => $enabled) {
    // if the vocab is enabled for filtering.
    if ($enabled) {

      // Load up the selected terms.
      $selected_terms = $conf[$conf['bundle'] . '_' . $field . '_terms'];

      // If child terms are required
      if ($conf['children'] == 1) {
        $child_terms =array();
        $tids = array();
        foreach ($selected_terms as $key => $value) {
          $parent_term = taxonomy_term_load($value);

          $child_terms += taxonomy_get_tree($parent_term->vid, $parent_term->tid);
          $tids[] = $parent_term->tid;
        }


        foreach ($child_terms as $key => $value) {
          $tids[] = $value->tid;
        }

      }
      else {

        // otherwise just filter by the selected.
        $tids = $selected_terms;
      }


      // Add the query if we need to
      $query->fieldCondition($field, 'tid', $tids, 'IN');

    }
  }


  // Add the range
  $query->range($conf['offset'], $conf['qty']);
  $query->propertyOrderBy('sticky', 'DESC');

  // Sort by created date
  if (isset($conf['order']) && $conf['order'] != '' && $conf['sorton'] !== 'published_at') {
    $query->propertyOrderBy('created', $conf['order'] );
  }
  else {
    if(!empty($conf['order'])){
      $query->addTag('published_at_' . $conf['order']);
    }
  }




  // Build the entity field query
  try {

    //

    $result = $query->execute();
    if (isset($result['node'])) {
      $nids = array_keys($result['node']);
      $nodes = node_load_multiple($nids);
      $block->content = node_view_multiple($nodes, $conf[$conf['bundle'] . '_view_mode']);
    }
  }
  catch (Exception $e) {
    watchdog('WIDGET PACK', var_export($e, true));
  }


  return $block;
}

/**
 * @return array
 */
function _widget_pack_list_enabled_bundles() {
  // load the settings array
  $settings = variable_get('widget_pack');

  $types_array = node_type_get_types();

  $active_bundles = array();
  foreach ($types_array as $key => $value) {
    if ($settings['content'][$key]['list']['enable']) {
      $active_bundles[$key] = $types_array[$key]->name;
    }
  }

  return $active_bundles;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function widget_pack_list_content_type_edit_form($form, &$form_state) {

  // Load the saved settings.  
  $conf = $form_state['conf'];

  // Get all the nodes types in the system.
  $types_array = node_type_get_types();

  // Extract the machine names  
  $types = array_keys($types_array);

  // Load up everything we know about registered entities.  
  $entity_info = entity_get_info();

  // Sort the view modes available to nodes by their machine names.
  ksort($entity_info['node']['view modes']);


  //  $bundle_values = array_keys(entity_view_mode_get_enabled_bundles('node', $key));

  // entity_view_mode_get_enabled_bundles($entity_type, $view_mode_name)

  /**
   * Content Types
   */

  // Load all the bundles that are enabled at admin/config/widgets  
  $active_bundles = _widget_pack_enabled_bundles('list');

  // If there are no bundles then direct people to the admin settings page.
  if (empty($active_bundles)) {
    $form['empty'] = array(
      '#markup' => t('Please visit admin/config/widgets and enable at least one node bundle.')
    );
    return $form;
  }

  // Create a select box for the active bundles
  $form['bundle'] = array(
    '#title' => 'Select a type of content.',
    '#type' => 'select',
    '#options' => $active_bundles,
    '#default_value' => $conf['bundle'],
  );

  // Loop through the bundles and create a vocabulary select box for each one.
  foreach ($active_bundles as $bundle => $type) {

    // Find out what vocabularies are in use by this content type.
    $vocab_types = array();
    $vocab_types += _widget_pack_enabled_vocabularies($bundle, 'list');

    $form[$bundle . '_vocab'] = array(
      '#title' => 'Filter by a vocabulary?',
      '#type' => 'checkboxes',
      '#options' => $vocab_types,
      '#default_value' => $conf[$bundle . '_vocab'],
      '#states' => array(
        'visible' => array(
          'select[name="bundle"]' => array('value' => $bundle),
        ),
      ),
    );

    foreach ($vocab_types as $vocab_type_key => $vocab_type_value) {
      if ($vocab_type_key != 'none') {

        $options = array();

        // Load the vocabulary

        $field_instance = field_info_instance('node', $vocab_type_key, $bundle);


        // Provide an autocomplete for selecting a poll.
        $form[$bundle . '_' . $vocab_type_key . '_terms'] = array(
          '#type' => 'select',
          '#title' => 'Choose a category of <em>' . $field_instance['label'] . '</em>.',
          '#description' => 'Select one or more cateogries to filter the list of content by.',
          '#options' => _widget_pack_vocabulary_generate_select_list($vocab_type_key),
          '#default_value' => $conf[$bundle . '_' . $vocab_type_key . '_terms'],
          '#multiple' => TRUE,
          '#states' => array(
            'visible' => array(
              'input[name="' . $bundle .'_vocab[' . $vocab_type_key . ']"]' => array('checked' => TRUE),
              'select[name="bundle"]' => array('value' => $bundle)
            ),
          ),
        );
      }
    }


    $view_modes = array();
    foreach ($entity_info['node']['view modes'] as $view_mode => $view_mode_info) {
      $bundle_values = array_keys(entity_view_mode_get_enabled_bundles('node', $view_mode));
      if (in_array($bundle, $bundle_values)) {
        $view_modes[$view_mode] = $view_mode;
      }
    }


    $form[$bundle . '_view_mode'] = array(
      '#title' => 'Select a view mode',
      '#type' => 'select',
      '#options' => _widget_pack_enabled_view_modes($bundle, 'list'),
      '#default_value' => $conf[$bundle . '_view_mode'],
      '#states' => array(
        'visible' => array(
          'select[name="bundle"]' => array('value' => $bundle),
        ),
      ),
    );
  }



  $form['children'] = array(
    '#title' => 'Include child terms?',
    '#type' => 'checkbox',
    '#default_value' => $conf['children']
  );

  $form['qty'] = array(
    '#title' => 'Select a number of items to display',
    '#type' => 'select',
    '#options' => range(0, 20),
    '#default_value' => $conf['qty']
  );

  $form['offset'] = array(
    '#title' => 'Select an offset',
    '#type' => 'select',
    '#options' => range(0, 20),
    '#default_value' => $conf['offset']
  );

  $form['sorton'] = array(
    '#title' => 'Sort on',
    '#type' => 'select',
    '#options' => array(
      'created' => 'Created date',
      'changed' => 'Changed date',
      'published_at' => 'Published date'

    ),
    '#default_value' => $conf['sorton']
  );

  // Add an option to sort by the publication date modules published_at field.
  if (module_exists('publication_date')) {
    $form['sorton']['#options']['published_at'] = 'Published date';
  }

  $form['order'] = array(
    '#title' => 'Ordering',
    '#type' => 'select',
    '#options' => array(
      '' => 'None',
      'asc' => 'Ascending',
      'desc' => 'Descending'
    ),
    '#default_value' => $conf['order']
  );


  // return the renderable array.
  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function widget_pack_list_content_type_edit_form_submit(&$form, &$form_state) {
  $keys = array('bundle', 'qty', 'offset', 'children', 'order', 'sorton');
  $active_bundles = _widget_pack_list_enabled_bundles();

  foreach ($active_bundles as $bundle => $value) {
    $keys[] = $bundle . '_vocab';
    $keys[] = $bundle . '_view_mode';
    $vocab_types = _widget_pack_enabled_vocabularies($bundle, 'list');
    foreach ($vocab_types as $vocab_type_key => $vocab_type_value) {
      $keys[] = $bundle . '_' . $vocab_type_key . '_terms';
    }
  }


  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


/**
 * Ctools plugin function hook_admin_info.
 */
function widget_pack_list_admin_info($subtype, $conf, $context = NULL) {

  if (empty($output) || !is_object($output)) {
     $output =  widget_pack_list_content_type_render($subtype, $conf, $args, $context);
     $output->title = 'List of Content';
  }
  return $output;
}
