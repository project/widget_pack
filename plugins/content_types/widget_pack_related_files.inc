<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Related files'),
  'description' => t('Show this pages related files.'),
  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('widget_pack_related_files_content_type'),
  // Name of a function which will render the block.
  'render callback' => 'widget_pack_related_files_content_type_render',
  // The default context.
  'defaults' => array(),
  'admin info' => 'widget_pack_related_files_admin_info',
  // This explicitly declares the config form. Without this line, the func would be
  // ctools_plugin_example_no_context_content_type_edit_form.
  'edit form' => 'widget_pack_related_files_content_type_edit_form',
  'category' => array(t('Widgets'), -9),
  // this example does not provide 'admin info', which would populate the
  // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function widget_pack_related_files_content_type_render($subtype, $conf, $args, $context) {

  // Build a blank block.
  $block = new stdClass();
  $block->content = NULL;

  // Load the node and check we have an enabled widget for the current node type.
  $node = menu_get_object();
  if (is_object($node) && $node->type == $conf['bundle'] && !empty($conf[$conf['bundle'] . '_reference']) && !empty($conf[$conf['bundle'] . '_view_mode'])) {

    $links = field_view_field('node', $node, $conf[$conf['bundle'] . '_reference'],  $conf[$conf['bundle'] . '_view_mode']);

    if ($links) {
      $block->content = $links;
      return $block;
    }
  }

  return NULL;

}

/**
 * @return array
 */
function _widget_pack_related_files_enabled_bundles() {
  // load the settings array
  $settings = variable_get('widget_pack');

  $types_array = node_type_get_types();

  $active_bundles = array();
  foreach ($types_array as $key => $value) {
    if ($settings['content'][$key]['related_files']['enable']) {
      $active_bundles[$key] = $types_array[$key]->name;
    }
  }

  return $active_bundles;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function widget_pack_related_files_content_type_edit_form($form, &$form_state) {

  // Load the saved settings.  
  $conf = $form_state['conf'];

  // Get all the nodes types in the system.
  $types_array = node_type_get_types();

  // Extract the machine names  
  $types = array_keys($types_array);

  // Load up everything we know about registered entities.  
  $entity_info = entity_get_info();

  // Sort the view modes available to nodes by their machine names.
  ksort($entity_info['node']['view modes']);


  //  $bundle_values = array_keys(entity_view_mode_get_enabled_bundles('node', $key));

  // entity_view_mode_get_enabled_bundles($entity_type, $view_mode_name)

  /**
   * Content Types
   */

  // Load all the bundles that are enabled at admin/config/widgets  
  $active_bundles = _widget_pack_enabled_bundles('related_files');

  // If there are no bundles then direct people to the admin settings page.
  if (empty($active_bundles)) {
    $form['empty'] = array(
      '#markup' => t('Please visit admin/config/widgets and enable at least one node bundle.')
    );
    return $form;
  }

  // Create a select box for the active bundles
  $form['bundle'] = array(
    '#title' => 'Select a type of content.',
    '#type' => 'select',
    '#options' => $active_bundles,
    '#default_value' => $conf['bundle'],
  );

  // Loop through the bundles and create a field select box for each one.
  foreach ($active_bundles as $bundle => $type) {

    // Find out what vocabularies are in use by this content type.
    $vocab_types = array('none' => 'Do not filter');
    $vocab_types += _widget_pack_enabled_references($bundle, 'related_files');

    $form[$bundle . '_reference'] = array(
      '#title' => 'Field',
      '#description' => 'Choose the field that referenced the related content',
      '#type' => 'select',
      '#options' => $vocab_types,
      '#default_value' => $conf[$bundle . '_reference'],
      '#states' => array(
        'visible' => array(
          'select[name="bundle"]' => array('value' => $bundle),
        ),
      ),
    );


    $view_modes = array();
    foreach ($entity_info['node']['view modes'] as $view_mode => $view_mode_info) {
      $bundle_values = array_keys(entity_view_mode_get_enabled_bundles('node', $view_mode));
      if (in_array($bundle, $bundle_values)) {
        $view_modes[$view_mode] = $view_mode;
      }
    }


    $form[$bundle . '_view_mode'] = array(
      '#title' => 'Select a view mode',
      '#type' => 'select',
      '#options' => _widget_pack_enabled_view_modes($bundle, 'related_files'),
      '#default_value' => $conf[$bundle . '_view_mode'],
      '#states' => array(
        'visible' => array(
          'select[name="bundle"]' => array('value' => $bundle),
        ),
      ),
    );
  }


  // return the renderable array.
  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function widget_pack_related_files_content_type_edit_form_submit(&$form, &$form_state) {
  $keys = array('bundle');
  $active_bundles = _widget_pack_related_files_enabled_bundles();

  foreach ($active_bundles as $bundle => $value) {
    $keys[] = $bundle . '_reference';
    $keys[] = $bundle . '_view_mode';

  }


  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


/**
 * Ctools plugin function hook_admin_info.
 */
function widget_pack_related_files_admin_info($subtype, $conf, $context = NULL) {

  if (empty($output) || !is_object($output)) {
    $output = new stdClass();
//    $node = node_load($conf[$conf['bundle'] . '_type']);

 //   $output->title = $conf['bundle'] . ' : ' . $node->title . '';
    $output->content = t('No info available.');
  }

  return $output;
}

