<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
    'title' => t('markup Embed'),
    'description' => t('Add an markup to a page'),
    // 'single' => TRUE means has no subtypes.
    'single' => TRUE,
    // Constructor.
    'content_types' => array('widget_pack_markup_content_type'),
    // Name of a function which will render the block.
    'render callback' => 'widget_pack_markup_content_type_render',
    // The default context.
    'defaults' => array(),
    'admin info' => 'widget_pack_markup_admin_info',
    // This explicitly declares the config form. Without this line, the func would be
    // ctools_plugin_example_no_context_content_type_edit_form.
    'edit form' => 'widget_pack_markup_content_type_edit_form',
    'category' => array(t('Widgets'), -9),
        // this example does not provide 'admin info', which would populate the
        // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function widget_pack_markup_content_type_render($subtype, $conf, $args, $context) {
  global $language;
  $block = new stdClass();
  $form['loading'] = array(
    '#markup' => '<div id="markup-loading-wrapper"> Loading... <br/><img src="/' . drupal_get_path('module','widget_pack_markup') . '/images/ajax-loader.gif" class="markup-loading" /></div>'
  );
  $form['content'] = array(
    '#markup' => $conf[$language->language]
  );
  $form['#attached']['js'][" jQuery(document).ready(function () {
        jQuery('markup').on('load', function () {
            jQuery('#markup-loading-wrapper').delay( 2000 ).fadeOut();
        });
    });"] = array( 'type' => 'inline');

  $block->content =  $form;
  return $block;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function widget_pack_markup_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $languages = language_list();
  foreach ($languages as $key => $value) {




    $wysiwyg = array(
      'value' => '', 'format' => 'filtered_html'
    );

    if (isset($conf[$key]) && is_array($conf[$key])) {
      $wysiwyg += $conf[$key];
    }


    $form[$key] = array(
      '#type' => 'text_format',
      '#format' => $wysiwyg['format'],
      '#default_value' => $wysiwyg['value'],
      '#title' => t($value->name),
      '#default_value' => $conf[$key]
    );
  }
  $form['override_title']['#access'] = false;
  $form['override_title_text']['#access'] = false;
  $form['override_title_markup']['#access'] = false;
  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function widget_pack_markup_content_type_edit_form_submit(&$form, &$form_state) {
  $languages = language_list();
  $keys = array_keys($languages);
  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Ctools plugin function hook_admin_info.
 */
function widget_pack_markup_admin_info($subtype, $conf, $context = NULL) {



  if (empty($output) || !is_object($output)) {
    $output = new stdClass();
    $output->title = 'You have selected a call to action: ' ;
    $output->content = t('No info available.');
  }

  return $output;
}

