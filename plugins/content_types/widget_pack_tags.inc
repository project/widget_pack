<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Insert tags list'),
  'description' => t('Insert a tag list.'),
  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('widget_pack_tags_content_type'),
  // Name of a function which will render the block.
  'render callback' => 'widget_pack_tags_content_type_render',
  // The default context.
  'defaults' => array(),
  'admin info' => 'widget_pack_tags_admin_info',
  // This explicitly declares the config form. Without this line, the func would be
  // ctools_plugin_example_no_context_content_type_edit_form.
  'edit form' => 'widget_pack_tags_content_type_edit_form',
  'category' => array(t('Widgets'), -9),
  // this example does not provide 'admin info', which would populate the
  // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function widget_pack_tags_content_type_render($subtype, $conf, $args, $context) {
  global $language;

  $block = new stdClass();




  $depth = 0;

  $vocab = taxonomy_vocabulary_machine_name_load($conf['bundle']);

  $tree = taxonomy_get_tree($conf['bundle']);

  $output = '';

  $output .= '<ul>';
  foreach ($tree as $term) {
    if ($term->depth > $depth) {
      $output .= '<ul>';
      $depth = $term->depth;
    }
    if ($term->depth < $depth) {
      $output .= '</ul>';
      $depth = $term->depth;
    }
    $output .= '<li>' . l($term->name, 'taxonomy/term/' . $term->tid) . '</li>';
  }
  $output .= '</ul>';


  $block->content = $output;
  return $block;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function widget_pack_tags_content_type_edit_form($form, &$form_state) {

  // Load the saved settings.  
  $conf = $form_state['conf'];

  $vocabulary = taxonomy_get_vocabularies();
  $checklist_vocab_array = array(); /* Change to array('0' => '--none--'); if you want a none option*/
  foreach ($vocabulary as $item) {
    $key = $item->vid;
    $value = $item->name;
    $checklist_vocab_array[$key] = $value;
  }



//
//
  $form['bundle'] = array(
    '#title' => 'Choose content bundle',
    '#type' => 'select',
    '#options' => $checklist_vocab_array,
    '#default_value' => $conf['bundle']
  );


  $form['count'] = array(
    '#title' => 'Include post count?',
    '#description' => 'Optionally include a post count',
    '#type' => 'checkbox',
    '#default_value' => $conf['count']
  );





  // return the renderable array.
  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function widget_pack_tags_content_type_edit_form_submit(&$form, &$form_state) {
  $keys = array('bundle','count');


  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


/**
 * Ctools plugin function hook_admin_info.
 */
function widget_pack_tags_admin_info($subtype, $conf, $context = NULL) {

  if (empty($output) || !is_object($output)) {
    $output = new stdClass();
//    $node = node_load($conf[$conf['bundle'] . '_type']);

 //   $output->title = $conf['bundle'] . ' : ' . $node->title . '';
    $output->content = t('No info available.');
  }

  return $output;
}

