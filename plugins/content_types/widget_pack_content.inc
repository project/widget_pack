<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Content'),
  'description' => t('Add a piece of content to the page.'),
  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('widget_pack_content_content_type'),
  // Name of a function which will render the block.
  'render callback' => 'widget_pack_content_content_type_render',
  // The default context.
  'defaults' => array(),
  'admin info' => 'widget_pack_content_admin_info',
  // This explicitly declares the config form. Without this line, the func would be
  // ctools_plugin_example_no_context_content_type_edit_form.
  'edit form' => 'widget_pack_content_content_type_edit_form',
  'category' => array(t('Widgets'), -9),
  // this example does not provide 'admin info', which would populate the
  // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function widget_pack_content_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();

  $nid = entity_autocomplete_get_id($conf[$conf['bundle'] . '_type']);

  $node = node_load($nid);

  if ($node) {
    $block->content = node_view($node, $conf[$conf['bundle'] . '_view_mode']);
  }
  else {
    $block->content = NULL;
  }

  return $block;
}


/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function widget_pack_content_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];


  // Get all the nodes types
  $types_array = node_type_get_types();
  $types = array_keys($types_array);

  $entity_info = entity_get_info();

  // Sort view modes by machine name.
  ksort($entity_info['node']['view modes']);


  //  $bundle_values = array_keys(entity_view_mode_get_enabled_bundles('node', $key));

  // entity_view_mode_get_enabled_bundles($entity_type, $view_mode_name)

  $content_types = _widget_pack_enabled_bundles();

  $form['bundle'] = array(
    '#title' => 'Select a type of content to insert.',
    '#type' => 'select',
    '#options' => $content_types,
    '#default_value' => $conf['bundle'],
    '#atrributes' => array(
      'id' => 'bundle'
    ),
    '#id' => 'bundle'
  );


  foreach ($content_types as $key => $type) {
    $options = array();
    $query = db_select('node');
    $result = $query->fields('node', array('nid', 'title'))
      ->condition('type', $key)
      ->condition('status', 1)
      ->orderBy('title', 'ASC')
      ->execute();

    foreach ($result as $row) {
      $options[$row->nid] = $row->title . ' | nid:' . $row->nid;
    }

    $form[$key . '_type'] = array(
      '#type' => 'select',
      '#title' => 'Choose a piece of <em>' . $type . '</em> content.',
      '#default_value' => isset($conf[$key . '_type']) ? $conf[$key . '_type'] : '',
      '#options' => $options,
      '#states' => array(
        'visible' => array(
          'select[name="bundle"]' => array('value' => $key),
        ),
      ),
    );


    $view_modes = array();
    foreach ($entity_info['node']['view modes'] as $view_mode => $view_mode_info) {
      $bundle_values = array_keys(entity_view_mode_get_enabled_bundles('node', $view_mode));
      if (in_array($key, $bundle_values)) {
        $view_modes[$view_mode] = $view_mode;
      }
    }


    $form[$key . '_view_mode'] = array(
      '#title' => 'Select a view mode',
      '#type' => 'select',
      '#options' => _widget_pack_enabled_view_modes($key, 'content'),
      '#default_value' => isset($conf[$key . '_view_mode']) ? $conf[$key . '_view_mode'] : '',
      '#states' => array(
        'visible' => array(
          'select[name="bundle"]' => array('value' => $key),
        ),
      ),
    );
  }


  // We dont want a title
//    $form['override_title']['#access'] = FALSE;
//    $form['override_title_text']['#access'] = FALSE;
//    $form['override_title_markup']['#access'] = FALSE;

  // return the renderable array.
  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function widget_pack_content_content_type_edit_form_submit(&$form, &$form_state) {
  $keys = array('bundle');
  $content_types = _widget_pack_enabled_bundles();
  foreach ($content_types as $key => $value) {
    $keys[] = $key . '_type';
    $keys[] = $key . '_view_mode';
  }
  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


/**
 * Ctools plugin function hook_admin_info.
 */
function widget_pack_content_admin_info($subtype, $conf, $context = NULL) {

  if (empty($output) || !is_object($output)) {
    $output = new stdClass();
    $node = node_load($conf[$conf['bundle'] . '_type']);
    if (is_object($node)) {
      $output->title = ucwords($conf['bundle']) . ' : ' . $node->title . '';
      $output->content = t('No info available.');
    }

  }

  return $output;
}

