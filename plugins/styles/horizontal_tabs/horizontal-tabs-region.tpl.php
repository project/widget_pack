<?php if (!empty($title)) { ?>
  <h2><?php print t($title); ?></h2>
<?php } ?>
<!--desktop-->
<div class="tabbed-items">
  <ul class="nav nav-tabs  hidden-phone">
    <?php foreach ($variables['headers'] as $id => $header) {
      print '<li><a href="#tab-' . $id . '">' . $header . '<span class="element-invisible"> tab</span></a></li>';
    }
    ?>
  </ul>
  <div class="nav-tabs-underline hidden-phone"></div>

  <!--mobile-->
  <select class="selectpicker form-control tabs-mobile hidden-desktop"
          title="Panel Navigation Tabbed Interface">

    <?php foreach ($variables['headers'] as $id => $header) {
      print '<option value="#tab-' . $id . '">' . $header . '</option>';
    }
    ?>

  </select>

  <?php foreach ($variables['tabs'] as $id => $tab) {
    print '<div id="tab-' . $id . '">' . $tab . '</div>';
  }
  ?>
</div>