<?php
/**
 * Implementation of hook_panels_styles().
 */
$plugin = array(
  'horizontal_tabs' => array(
    'title' => t('Horizontal tabs'),
    'description' => t('objects will be rendered in a tabbed interface'),
    //'render pane' => 'tab_render_pane',
    'render region' => 'horizontal_tabs_render_region',
    //'pane settings form' => 'tab_settings_form',
    'hook theme' => array(
      'horizontal_tabs_theme_region' => array(
        'template' => 'horizontal-tabs-region',
        'path' => drupal_get_path('module', 'widget_pack') . '/plugins/styles/horizontal_tabs',
        'variables' => array(
          'headers' => NULL,
          'tabs' => NULL,
        ),
      ),
    ),
  ),
);


function theme_horizontal_tabs_render_region($vars) {
  $tabs = array();
  $headers = array();

  // Save any panes with horizontal rule as we dont want them in a tab,
  $horizontal_rule_panes = array();

  foreach ($vars['panes'] as $pane_id => $pane_output) {
    if (strrpos($pane_output, 'horizontal-rule') !== FALSE) {
      $horizontal_rule_panes[] = $pane_id;
      continue;
    }

    if (!empty($vars['display']->content[$pane_id]->configuration['override_title_text']) && $vars['display']->content[$pane_id]->configuration['override_title'] == 1) {
      $title = t($vars['display']->content[$pane_id]->configuration['override_title_text']);
    }
    else {
      $title = 'no title please set';
    }
    $headers[$pane_id] = $title;
    $tabs[$pane_id] = $pane_output;

  }
  if (empty($tabs)) {
    return;
  }

  $rendered_tabs = theme('horizontal_tabs_theme_region', array(
    'headers' => $headers,
    'tabs' => $tabs
  ));

  $rendered_horizontal_rules = '';
  foreach ($horizontal_rule_panes as $key => $pane_id) {
    $rendered_horizontal_rules .= $vars['panes'][$pane_id];
  }

  return $rendered_tabs . $rendered_horizontal_rules;
}
